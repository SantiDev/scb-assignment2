package th.in.santi.scb.game.type;

import th.in.santi.scb.game.enu.Result;

public interface PlayerType {
	

	
	public Result getResult(PlayerType otherPlayer);

}
