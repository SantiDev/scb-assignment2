package th.in.santi.scb.game.type;

import th.in.santi.scb.game.enu.Result;

public class PaperType implements PlayerType {

	@Override
	public Result getResult(PlayerType otherPlayer) {
		if (otherPlayer == null || otherPlayer instanceof RockType)
			return Result.WINS;
		if (otherPlayer instanceof ScissorType)
			return Result.LOSE;
		return Result.DRAW;
	}

}
