package th.in.santi.scb.game.type;

import th.in.santi.scb.game.enu.Result;

public class ScissorType implements PlayerType {

	@Override
	public Result getResult(PlayerType otherPlayer) {
		if (otherPlayer == null || otherPlayer instanceof PaperType)
			return Result.WINS;
		if (otherPlayer instanceof RockType)
			return Result.LOSE;
		return Result.DRAW;
	}

}