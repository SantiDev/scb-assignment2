package th.in.santi.scb.game.type;

import th.in.santi.scb.game.enu.Result;

public class RockType implements PlayerType {

	@Override
	public Result getResult(PlayerType otherPlayer) {
		if (otherPlayer == null || otherPlayer instanceof ScissorType)
			return Result.WINS;
		if (otherPlayer instanceof PaperType)
			return Result.LOSE;
		return Result.DRAW;
	}

}
