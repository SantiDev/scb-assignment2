package th.in.santi.scb.game.utils;

import th.in.santi.scb.game.type.PaperType;
import th.in.santi.scb.game.type.PlayerType;
import th.in.santi.scb.game.type.RockType;
import th.in.santi.scb.game.type.ScissorType;

public class Utils {
	public static PlayerType createPlayerType(String input) {
		if (input == null)
			return null;
		input = input.toUpperCase();
		switch (input) {
		case "R":
		case "ROCK":
			return new RockType();
		case "S":
		case "SCISSOR":
			return new ScissorType();
		case "P":
		case "PAPER":
			return new PaperType();
		}
		return null;
	}
}
