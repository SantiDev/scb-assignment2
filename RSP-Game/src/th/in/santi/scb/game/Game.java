package th.in.santi.scb.game;

import java.util.InputMismatchException;
import java.util.Scanner;

import th.in.santi.scb.game.enu.Result;
import th.in.santi.scb.game.type.PlayerType;
import th.in.santi.scb.game.utils.Utils;

public class Game {

	private final String REGEX_PLAYGAME = "^(Rock|Scissor|Paper)( )(Rock|Scissor|Paper)$";
	private final String REGEX_PLAY_ROUND = "^[1-9][0-9]*$";
	private int player1win = 0;
	private int player2win = 0;

	public static void main(String[] args) {
		Game game = new Game();
		game.startGame();
	}

	public void startGame() {
		Scanner scanner = new Scanner(System.in);
		try {
			System.out.println("Input:");
			int playRound = getPlayRound(scanner.nextLine());
			String[] output = new String[playRound];
			int idx = 0;
			while (idx < playRound) {
				output[idx++] = getPlayerResult(scanner.nextLine());
			}

			System.out.println("\nOutput:");
			for (String out : output) {
				System.out.println(out);
			}

			System.out.println("Winner: " + (player1win > player2win ? "Player 1"
					: player2win > player1win ? "Player 2" : Result.DRAW.toString()));
		} catch (InputMismatchException e) {
			System.out.println(e.getMessage());
		} finally {
			scanner.close();
		}
	}

	public int getPlayRound(String input) {
		int playRound = 0;
		while (playRound < 1) {
			if (input != null && input.matches(REGEX_PLAY_ROUND)) {
				playRound = Integer.parseInt(input);
			} else {
				throw new InputMismatchException(Result.getErrorInput());
			}
		}
		return playRound;
	}

	public String getPlayerResult(String input) {
		if (input != null && input.trim().matches(REGEX_PLAYGAME)) {
			String[] tmp_player = input.trim().split(" ");
			PlayerType player1 = Utils.createPlayerType(tmp_player[0]);
			PlayerType player2 = Utils.createPlayerType(tmp_player[1]);
			Result result = player1.getResult(player2);
			switch (result) {
			case WINS:
				player1win++;
				return Result.getPlayerWin(1);
			case LOSE:
				player2win++;
				return Result.getPlayerWin(2);
			case DRAW:
				return result.toString();
			}
		}
		throw new InputMismatchException(Result.getErrorInput());
	}

}
