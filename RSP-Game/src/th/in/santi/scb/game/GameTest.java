package th.in.santi.scb.game;

import static org.junit.Assert.assertEquals;

import java.util.InputMismatchException;

import org.junit.BeforeClass;
import org.junit.Test;

import th.in.santi.scb.game.enu.Result;
import th.in.santi.scb.game.type.PlayerType;
import th.in.santi.scb.game.utils.Utils;

public class GameTest {
	static PlayerType rock;
	static PlayerType scissor;
	static PlayerType paper;
	static Game game;

	@BeforeClass
	public static void initGameTest() {
		rock = Utils.createPlayerType("Rock");
		scissor = Utils.createPlayerType("Scissor");
		paper = Utils.createPlayerType("Paper");
		game = new Game();
	}

	@Test
	public void testRockVsScissor() {
		Result result = rock.getResult(scissor);
		System.out.println("Rock " + result + " Scissor!");
		assertEquals(result, Result.WINS);
	}

	@Test
	public void testRockVsPaper() {
		Result result = rock.getResult(paper);
		System.out.println("Rock " + result + " Paper!");
		assertEquals(result, Result.LOSE);
	}

	@Test
	public void testRockVsRock() {
		Result result = rock.getResult(rock);
		System.out.println("Rock " + result + " Rock!");
		assertEquals(result, Result.DRAW);
	}

	@Test
	public void testScissorVsPaper() {
		Result result = scissor.getResult(paper);
		System.out.println("Scissor " + result + " Paper!");
		assertEquals(result, Result.WINS);
	}

	@Test
	public void testScissorVsRock() {
		Result result = scissor.getResult(rock);
		System.out.println("Scissor " + result + " Rock!");
		assertEquals(result, Result.LOSE);
	}

	@Test
	public void testScissorVsScissor() {
		Result result = scissor.getResult(scissor);
		System.out.println("Scissor " + result + " Scissor!");
		assertEquals(result, Result.DRAW);
	}

	@Test
	public void testPaperVsRock() {
		Result result = paper.getResult(rock);
		System.out.println("Paper " + result + " Rock!");
		assertEquals(result, Result.WINS);
	}

	@Test
	public void testPaperVsScissor() {
		Result result = paper.getResult(scissor);
		System.out.println("Paper " + result + " Scissor!");
		assertEquals(result, Result.LOSE);
	}

	@Test
	public void testPaperVsPaper() {
		Result result = paper.getResult(paper);
		System.out.println("Paper " + result + " Paper!");
		assertEquals(result, Result.DRAW);
	}

	@Test
	public void testInputRoundSuccess() {
		assertEquals(game.getPlayRound("5"), 5);
	}

	@Test
	public void testInputRoundFail() {
		try {
			game.getPlayRound("0");
		} catch (InputMismatchException e) {
			assertEquals(e.getMessage(), Result.getErrorInput());
		}

		try {
			game.getPlayRound("a");
		} catch (InputMismatchException e) {
			assertEquals(e.getMessage(), Result.getErrorInput());
		}

		try {
			game.getPlayRound("");
		} catch (InputMismatchException e) {
			assertEquals(e.getMessage(), Result.getErrorInput());
		}
	}

	@Test
	public void testInputPlayer1Win() {
		assertEquals(game.getPlayerResult("Scissor Paper"), Result.getPlayerWin(1));
		assertEquals(game.getPlayerResult("Rock Scissor"), Result.getPlayerWin(1));
		assertEquals(game.getPlayerResult("Paper Rock"), Result.getPlayerWin(1));
	}

	@Test
	public void testInputPlayer2Win() {
		assertEquals(game.getPlayerResult("Paper Scissor"), Result.getPlayerWin(2));
		assertEquals(game.getPlayerResult("Scissor Rock"), Result.getPlayerWin(2));
		assertEquals(game.getPlayerResult("Rock Paper"), Result.getPlayerWin(2));
	}

	@Test
	public void testInputPlayerDraw() {
		assertEquals(game.getPlayerResult("Paper Paper"), Result.DRAW.toString());
		assertEquals(game.getPlayerResult("Scissor Scissor"), Result.DRAW.toString());
		assertEquals(game.getPlayerResult("Rock Rock"), Result.DRAW.toString());
	}

	@Test
	public void testInputPlayerFail() {
		boolean INPUT_SUCCESS = true;
		try {
			game.getPlayerResult("");
			
			assertEquals(false, INPUT_SUCCESS);
		} catch (InputMismatchException e) {
		
			assertEquals(e.getMessage(), Result.getErrorInput());
		}

		try {
			game.getPlayerResult(" ");
			assertEquals(false, INPUT_SUCCESS);
		} catch (InputMismatchException e) {
			assertEquals(e.getMessage(), Result.getErrorInput());
		}

		try {
			game.getPlayerResult("Paper");
			assertEquals(false, INPUT_SUCCESS);
		} catch (InputMismatchException e) {
			assertEquals(e.getMessage(), Result.getErrorInput());
		}
	
	}

}
