package th.in.santi.scb.game.enu;

public enum Result {
	DRAW("Draw"), WINS("Wins"), LOSE("Lose");

	String result;

	Result(String result) {
		this.result = result;
	}

	@Override
	public String toString() {
		return result;
	}

	public static String getPlayerWin(int player) {
		return "Player " + player + " wins";
	}

	public static String getErrorInput() {
		return "Invalid input: Matches do not tally!";
	}

}
